package bankbazaartest;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankbazzarpages.CompareAndApplyLoans;
import wdMethods.ProjectMethods;

public class classWork extends ProjectMethods{
	
	@BeforeClass
	public void setData() {
		testCaseName = "TC001_BankBazzar";
		testCaseDescription ="Mutual Fund";
		category = "Smoke";
		author= "mano";
		dataSheetName="BankBazarSheet";
	}
	
	@Test(dataProvider="fetchData")
	public void test1(String age,String Month,String date,String salary,String bank,String name) throws InterruptedException {
		startApp("chrome", "https://www.bankbazaar.com");
		new CompareAndApplyLoans().moveToInvestment()
		.selectMutualFunds()
		.selectSearchMutualFunds()
		.selectAge(Integer.parseInt(age))
		.selectMonthYear(Month)
		.selectDay(date)
		.selectContinue()
		.selectSalary(salary)
		.selectContinue()
		.selectBank(bank)
		.enterName(name)
		.selectViewMutualfunds()
		.getSchemeListAndAmount();

		
	}
	

}
