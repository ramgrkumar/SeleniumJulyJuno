package bankbazzarpages;

import org.openqa.selenium.interactions.Actions;

import wdMethods.ProjectMethods;

public class AnnulIncomePage extends ProjectMethods{
	
public AnnulIncomePage selectSalary(String salary) throws InterruptedException {
	Thread.sleep(3000);
		Actions builder = new Actions(driver);
		//builder.dragAndDrop(locateElement("linkText","18"), locateElement("linkText", age));
		String currentSalaryString = getText(locateElement("xpath","//div[@class='rangeslider__handle-label']"));
		/*currentSalaryString.replaceAll("//D", "");
		System.out.println("Salary in string" + currentSalaryString);
		int currentSalary=Integer.parseInt(currentSalaryString);
		System.out.println("Salary in int" + currentSalary);
*/		
		//int currentSalary=Integer.parseInt(getText(locateElement("xpath","//div[@class='rangeslider__handle-label']")));
		
		while(!(currentSalaryString.equals(salary)) ) {
		builder.dragAndDropBy(locateElement("xpath","//div[@class='rangeslider__handle']"), 6, 0);
		builder.perform();
		currentSalaryString = getText(locateElement("xpath","//div[@class='rangeslider__handle-label']"));
		if(currentSalaryString.equals(salary)) {
			break;
		}
	    //System.out.println("Salary in int" + currentSalary);
		
		}
		
		return this;
		
	}
	

   public BankSelectionPage selectContinue() {
	click(locateElement("linktext","Continue"));
	return new BankSelectionPage();
    }
	
	

}
