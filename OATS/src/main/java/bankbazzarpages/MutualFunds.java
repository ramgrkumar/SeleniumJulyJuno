package bankbazzarpages;

import wdMethods.ProjectMethods;

public class MutualFunds extends ProjectMethods {
	
	public AgeSelectionPage selectSearchMutualFunds() throws InterruptedException {
		
		Thread.sleep(3000);
		//Search for Mutual Funds
		click(locateElement("linktext","Search for Mutual Funds"));
		return new AgeSelectionPage();
	}
	
	 

}
