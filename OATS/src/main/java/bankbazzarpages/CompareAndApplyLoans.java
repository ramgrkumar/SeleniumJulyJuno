package bankbazzarpages;

import wdMethods.ProjectMethods;
import org.openqa.selenium.interactions.Actions;

public class CompareAndApplyLoans  extends ProjectMethods{
	
	public CompareAndApplyLoans moveToInvestment() {
		
		Actions builder= new Actions(driver);
		builder.moveToElement(locateElement("linktext","INVESTMENTS"));
		builder.perform();
		return this;
		
	}
	
	//Mutual Funds
	public MutualFunds selectMutualFunds() {
		//locateElement("linktext","Mutual Funds");
		click(locateElement("linktext","Mutual Funds"));
		return new MutualFunds();
	}

}
