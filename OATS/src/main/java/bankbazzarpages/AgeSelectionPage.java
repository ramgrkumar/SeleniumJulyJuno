package bankbazzarpages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import wdMethods.ProjectMethods;

public class AgeSelectionPage extends ProjectMethods {
	
	public AgeSelectionPage selectAge(int age) throws InterruptedException {
		
		Thread.sleep(3000);
		Actions builder = new Actions(driver);
		//builder.dragAndDrop(locateElement("linkText","18"), locateElement("linkText", age));
		int currentAge=Integer.parseInt(getText(locateElement("xpath","//div[@class='rangeslider__handle-label']")));
		
		while(currentAge < age) {
		builder.dragAndDropBy(locateElement("xpath","//div[@class='rangeslider__handle']"), 8, 0);
		builder.perform();
		currentAge=Integer.parseInt(getText(locateElement("xpath","//div[@class='rangeslider__handle-label']")));
		
		}
		
		return this;
		
	}
	
	public AgeSelectionPage selectMonthYear(String month) throws InterruptedException {
		
		Thread.sleep(3000);
		click(locateElement("linktext",month));
		return this;
	}
	
	public AgeSelectionPage selectDay(String day) throws InterruptedException {
		Thread.sleep(3000);
    List<WebElement> listDays = driver.findElementsByXPath("//div[text()='"+day+"']");
    if(listDays.size()>1) {
		click(locateElement("xpath","(//div[text()='"+day+"'])[2]"));
		return this;
    }
    else {
    	click(locateElement("xpath","(//div[text()='"+day+"'])[1]"));
		return this;
    }
	}
	
	public AnnulIncomePage selectContinue() {
		click(locateElement("linktext","Continue"));
		return new AnnulIncomePage();
	}

}
