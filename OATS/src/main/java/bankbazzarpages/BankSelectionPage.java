package bankbazzarpages;

import wdMethods.ProjectMethods;

public class BankSelectionPage extends ProjectMethods {
	
	public EnterDetailsPage selectBank(String bankname) throws InterruptedException {
		Thread.sleep(3000);
		click(locateElement("xpath", "//span[text()='"+bankname+"']/following::span/input"));
		return new EnterDetailsPage();
	}

}
