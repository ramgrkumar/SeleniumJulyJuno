package bankbazzarpages;

import org.openqa.selenium.interactions.SendKeysAction;

import wdMethods.ProjectMethods;

public class EnterDetailsPage extends ProjectMethods {
	
	public EnterDetailsPage enterName(String name) throws InterruptedException {
		Thread.sleep(3000);
		type(locateElement("xpath", "//input[@name='firstName']"), name);
		return this;
	}
   //View Mutual Funds
	public DisplayInvenstment selectViewMutualfunds() {
		click(locateElement("linktext","View Mutual Funds" ));
		 return new DisplayInvenstment();
		
		}
}
