package week1day1Assignment;

import java.util.Scanner;

//find mobile operator by user input

public class practice3 {

	public static void main(String[] args) {
	
		System.out.println("Enter first three digits of phone number");
		Scanner number = new Scanner(System.in);
		int input =  number.nextInt();
		
		if (input == 944) {
			System.out.println("Your operator is BSNL");
		}
		else if (input == 900) {
			System.out.println("Your operator is Airtel");
		}
		else if (input == 897){
			System.out.println("Your operator is IDEA");
		}
		
		else if (input == 630){
			System.out.println("Your operator is JIO");
		}
		
		else {
			System.out.println("Enter a valid number");
		}

	}

}