package week2day1;

public interface Television {
	
	public void channelSelection(int channelNumber);
	public void channelSelection(String channelName);
	public void setVolume(int volume);
	public void setColour (int colour);

}
