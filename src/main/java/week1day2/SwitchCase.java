package week1day2;

import java.util.Scanner;

public class SwitchCase {

	public static void main(String[] args) {

		System.out.println("Enter first three digits of phone number");
		Scanner number = new Scanner(System.in);
		int input =  number.nextInt();

		switch (input)
		{
		case 944:
			System.out.println("BSNL");

			break;

		case 900:
			System.out.println("AIRTEL");
			break;
		case 897:
			System.out.println("IDEA");
			break;

		case 630:
			System.out.println("JIO");
			break;

		default:
			System.out.println("Give a valid input");
		}



	}

}
