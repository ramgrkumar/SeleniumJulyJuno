package week3day1;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ClassWork1 {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main");
		try {
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
        driver.findElementById("createLeadForm_companyName").sendKeys("IBM");
         driver.findElementById("createLeadForm_firstName").sendKeys("Ram");
         driver.findElementById("createLeadForm_lastName").sendKeys("Kumar");
         driver.findElementByName("submitButton").click();
        
		}
		catch (NoSuchElementException e) {
			System.out.println("No such field");
			throw new RuntimeException();
		}
	}

}
