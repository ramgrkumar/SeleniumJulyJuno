package week3day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class HomeWork1 {

	public static void main(String[] args) {


		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().window().maximize();   
		driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("IBM");
		driver.findElementById("createLeadForm_firstName").sendKeys("Ram");
		driver.findElementById("createLeadForm_lastName").sendKeys("Kumar");
		WebElement src = driver.findElementById("createLeadForm_dataSourceId");
		Select dropSource = new Select(src);
		dropSource.selectByVisibleText("Conference");      
		WebElement src2 = driver.findElementById("createLeadForm_marketingCampaignId");
		Select dropMarketing = new Select(src2);       
		dropMarketing.selectByValue("CATRQ_CARNDRIVER");
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Mano");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("Raja");
		driver.findElementByName("personalTitle").sendKeys("Mr.");
		driver.findElementByName("generalProfTitle").sendKeys("Engineer");
		driver.findElementById("createLeadForm_departmentName").sendKeys("Test Automation");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("2000000");
		WebElement src3 = driver.findElementById("createLeadForm_currencyUomId");
		Select currency=  new Select(src3);
		currency.selectByVisibleText("INR - Indian Rupee");
		WebElement src4 = driver.findElementById("createLeadForm_industryEnumId");	
		Select industry = new Select(src4);
		industry.selectByVisibleText("Telecommunications");
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("1000");
		WebElement src5 = driver.findElementById("createLeadForm_ownershipEnumId");
		Select owner = new Select(src5);
		owner.selectByVisibleText("Corporation");
		driver.findElementById("createLeadForm_sicCode").sendKeys("12312");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("9019209");
		driver.findElementById("createLeadForm_description").sendKeys("Selenium experts");
		driver.findElementById("createLeadForm_importantNote").sendKeys("Never underestimate the power of common man");
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("44");
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("7299990101");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("21010");
		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("Mr.Ramkumar");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("ramgrkumar@gmail.com");
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("www.beachview.com");
		driver.findElementById("createLeadForm_generalToName").sendKeys("Ramnath Kovind");
		driver.findElementById("createLeadForm_generalAttnName").sendKeys("Narendra Modi");
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("PM Office");
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("Parliament road");
		driver.findElementById("createLeadForm_generalCity").sendKeys("New Delhi");
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("623526");
		driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("1234");
		WebElement src6 = driver.findElementById("createLeadForm_generalCountryGeoId");
		Select country = new Select(src6);
		country.selectByVisibleText("India");
		WebElement src7 = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
		Select state = new Select(src7);
		state.selectByVisibleText("TAMILNADU");
		driver.findElementByName("submitButton").click();
	}
}

