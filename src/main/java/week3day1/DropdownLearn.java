package week3day1;

import java.util.List;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class DropdownLearn {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().window().maximize();   // maximise

		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
        driver.findElementById("createLeadForm_companyName").sendKeys("IBM");
         driver.findElementById("createLeadForm_firstName").sendKeys("Ram");
         driver.findElementById("createLeadForm_lastName").sendKeys("Kumar");
         WebElement src = driver.findElementById("createLeadForm_dataSourceId");

         Select dropSource = new Select(src);
         dropSource.selectByVisibleText("Conference");
       
         WebElement src2 = driver.findElementById("createLeadForm_marketingCampaignId");
         Select dropMarketing = new Select(src2);
        
         dropMarketing.selectByValue("CATRQ_CARNDRIVER");
         List<WebElement> options = dropMarketing.getOptions();
         for(WebElement eachOption: options) {
        	 System.out.println(eachOption.getText());
         }

         

		
	}

	}


