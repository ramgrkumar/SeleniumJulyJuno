package week4day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class HomeWork1 {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("(//img[@alt= 'Lookup'])[1]").click();
		Set<String> allwindow = driver.getWindowHandles();
		List<String> listwindow = new ArrayList<String>();
		listwindow.addAll(allwindow);
		String secondwindow = listwindow.get(1);
		driver.switchTo().window(secondwindow);
		driver.findElementByName("id").sendKeys("100");
		driver.findElementByXPath("//button[text() = 'Find Leads']").click();
		Thread.sleep(3000);
		driver.findElementByXPath("(//a[@class = 'linktext'])[1]").click();
		String firstwindow = listwindow.get(0);
		driver.switchTo().window(firstwindow);
		driver.findElementByXPath("(//img[@alt= 'Lookup'])[2]").click();
		Set<String> allwindow1 = driver.getWindowHandles();
		List<String> listwindow1 = new ArrayList<String>();
		listwindow1.addAll(allwindow1);
		String secondwindow1 = listwindow1.get(1);
		driver.switchTo().window(secondwindow1);
		driver.findElementByName("id").sendKeys("101");
		driver.findElementByXPath("//button[text() = 'Find Leads']").click();
		Thread.sleep(3000);
		driver.findElementByXPath("(//a[@class = 'linktext'])[1]").click();
		String firstwindow1 = listwindow.get(0);
		driver.switchTo().window(firstwindow1);
		driver.findElementByXPath("//a[@class = 'buttonDangerous']").click();
		driver.switchTo().alert().accept();
		String error = driver.findElementByClassName("errorMessage").getText();
		System.out.println(error);
		
	}

}

