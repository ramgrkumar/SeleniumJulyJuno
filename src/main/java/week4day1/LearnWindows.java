package week4day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWindows {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.irctc.co.in/eticketing/loginHome.jsf");
		driver.manage().window().maximize();
		Thread.sleep(3000);
		driver.findElementByXPath("//span[text()='AGENT LOGIN']").click();
		//driver.findElementByLinkText("AGENT LOGIN").click();
		//driver.findElementByLinkText("Contact Us").click();
		driver.findElementByXPath("//a[text() = 'Contact Us']").click();
		Set<String> allWindows = driver.getWindowHandles();
		System.out.println("Number of windows "+allWindows.size());
		List<String> allWindowList = new ArrayList<String>();
		 allWindowList.addAll(allWindows);
		driver.switchTo().window(allWindowList.get(1));
		System.out.println("Title of new window "+ driver.getTitle());
		System.out.println("URL of second window is "+ driver.getCurrentUrl());
		driver.close();
		Thread.sleep(2000);
		driver.switchTo().window(allWindowList.get(0));
		driver.close();
		
		//driver.quit();
		
		

	}

}
