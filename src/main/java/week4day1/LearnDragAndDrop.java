package week4day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class LearnDragAndDrop {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://jqueryui.com/draggable/");
driver.switchTo().frame(0);
WebElement drag = driver.findElementByXPath("//div[@id = 'draggable']/p");
Actions builder = new Actions (driver);
builder.dragAndDropBy(drag, 100, 100).perform();


	}

}
