package week4day1;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnFrameAlerts {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text() = 'Try it']").click();
		driver.switchTo().alert().sendKeys("Ramkumar");
		driver.switchTo().alert().accept();
		//driver.getScreenshotAs()
		String text = driver.findElementById("demo").getText();
		System.out.println("finally " + text);
	}
	

}
