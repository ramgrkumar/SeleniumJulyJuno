package Week1day1;

public class MobilePhone {
	static long phoneNumber;
	static String phoneMake;
	static int price;
	static float megaPixel;
	static boolean dualSim ;
	static char is4G ;

	static void phoneDetails() {

		phoneNumber = 9095182714l;
		phoneMake = "XIOMI";
		price = 8999;
		megaPixel = 16.1f;
		dualSim =   true;
		is4G = 'y';


	}

	static void lockPhone() {
		System.out.println("Phone is locked");
	}

	static void addContact(int phoneNumber, String firstName, String lastName) {
		System.out.println(phoneNumber + " added as a contact " +  firstName + lastName);
	}


	public static void call (long phoneNumber) {
		System.out.println("calling " + phoneNumber);
	}

	static void unLockPhone() {
		System.out.println("Phone is unlocked");
	}

	static boolean dualSimstatus() {
		return dualSim;
	}

	public static void sendMessage(String Message, long Recepient) {

		System.out.println("Your message " + Message +" is sent to " + Recepient );
	}

	static long getNumber() {
		return phoneNumber;
	}

	static void updateContact(String contactName, long newNumber) {
		System.out.println("The contact "+ contactName + " is updated");
	}
	
	static void deleteContact (String contactName) {
		System.out.println("The contact " + contactName + " is deleted successfully");	
	}
	
	static void flightMode () {
		System.out.println ("Switched to flight mode");
	}
	public static void main(String[] args) {
		lockPhone();
		addContact(90951, "Ram", "Kumar");
		addContact(4998, "Mano", "Raja");
		call(9095182714l);
		unLockPhone();
		phoneDetails();
		dualSimstatus();
		boolean result;
		result = dualSimstatus();
		System.out.println(result);
		long phoneNum;
		phoneNum = getNumber();
		System.out.println(phoneNum);
		sendMessage("Recharge your account", 9842865863l);
		updateContact("vijay", 8344297299l);
		deleteContact("Manmohan");
		flightMode();
	}

}