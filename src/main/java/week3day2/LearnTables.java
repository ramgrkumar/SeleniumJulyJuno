package week3day2;

import java.awt.RenderingHints.Key;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnTables {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://erail.in/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MAS",Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("SBC",Keys.TAB);
	
		
		 driver.findElementById("buttonFromTo").click();
		 
		 boolean value = driver.findElementById("chkSelectDateOnly").isSelected();
			
			if(value) {
				 driver.findElementById("chkSelectDateOnly").click();
			}
		 WebElement table = driver.findElementByXPath("//table[@class='DataTable TrainList']");
	     List<WebElement>  trow = table.findElements(By.tagName("tr"));
	    // List<WebElement> tcol = table.findElements(By.tagName("td"));
	    
	    
	    /* for (WebElement eachRow : tcol) {
		    	
		    	
		    	
		    	System.out.println(eachRow.getText());
				
			}
	     
	     System.out.println("try ends");
	     */
	     
	   for (WebElement eachRow : trow) {
	    	
	    	List<WebElement> tcol = eachRow.findElements(By.tagName("td"));
	    	
	    	
	    	System.out.println(tcol.get(1).getText());
			
		}
	    
	   
	     
		
		

	}

}
