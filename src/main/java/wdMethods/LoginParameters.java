package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class LoginParameters extends SeMethods {
	@BeforeClass(groups = "common")
	public void setData() {
		testCaseName="TC001";
		testCaseDescription="Login";
		author="Ram";
		category="Smoke";
	}
	
	
	@Test
@Parameters({"browser", "appurl", "username", "password"}) //order can change, but case sensitive with respect to xml
	public void login(String browserName, String url, String uname, String pass) {
		startApp(browserName,url);
		WebElement se1 = locateElement("id","username");
		type (se1,uname);
		 se1 = locateElement("id","password");
		 type (se1,pass);
		  WebElement login = locateElement("class","decorativeSubmit");
		 click(login);
		 WebElement crmsfa = locateElement("linkText","CRM/SFA");
		 click(crmsfa);
	}

}
