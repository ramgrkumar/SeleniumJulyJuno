package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
@Test
public class Facebook extends SeMethods {

	@BeforeClass
	public void setData() {
		testCaseName="TC002";
		testCaseDescription="Facebook";
		author="Ramkumar";
		category="Functional";
	}

	public void facebookTest( ) throws InterruptedException {

		startApp("chrome","https://www.facebook.com/");
		WebElement email = locateElement("id", "email");
		type (email, "ramgrkumar@gmail.com");
		WebElement pass = locateElement("id", "pass");
		type (pass, "97579757");
		WebElement login = locateElement("xpath", "//input[@data-testid = 'royal_login_button']");
		click(login);
		Thread.sleep(10);
		WebElement searchtext = locateElement("xpath", "//input[@aria-label = 'Search']");
		type(searchtext, "test leaf");
		Thread.sleep(10);
		WebElement search = locateElement("xpath", "//button[@data-testid = 'facebar_search_button']");
		click(search);
;
		WebElement testleaf = locateElement("xpath", "//div[text() = 'TestLeaf']");
		String text2 = getText(testleaf);
		if(text2.equalsIgnoreCase("testleaf"))
		{
			System.out.println("The page you expected is displayed");
		}
		else {
			System.out.println("The search result dont have your expected page");
		}


		WebElement like = locateElement("xpath", "(//button[@type = 'submit'])[2]");
		String text = getText(like);
		System.out.println("text");
		if (text.equalsIgnoreCase("liked")) {
			System.out.println("The page is already liked by you");
		}

		else {
			click(like);
		}

		WebElement testleaf1 = locateElement("xpath", "//div[text() = 'TestLeaf']");
		click(testleaf1);

		if (verifyTitle("TestLeaf"))
		{
			System.out.println("Title verifed");
		}
		else
		{
			System.out.println("Title not maching");
		}

		WebElement likesnumber = locateElement("xpath", "//div[contains(text(), 'people like this')]");
		String text3 = getText(likesnumber);
		System.out.println("The number of likes for TestLeaf is " + text3);
		//closeBrowser();







	}


}
