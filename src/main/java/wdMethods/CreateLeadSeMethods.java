package wdMethods;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CreateLeadSeMethods  extends ProjectMethods{
	
	@BeforeClass(groups = "common")
	public void setData() {
		testCaseName="TC001";
		testCaseDescription="Create lead";
		author="Ram";
		category="Smoke";
	}

	@Test(/*invocationCount=2, invocationTimeOut=50000*/ groups = "smoke")
	public void  testCreateLead () {
		//login();
		WebElement createlead = locateElement("linkText","Create Lead");
		click(createlead);


		type(locateElement("id", "createLeadForm_companyName"),"IBM");
		type(locateElement("id", "createLeadForm_firstName"),"IBM");
		type(locateElement("id", "createLeadForm_lastName"),"IBM");


		WebElement sourceDrop = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(sourceDrop, "Conference");
		WebElement marketingId = locateElement("id", "createLeadForm_marketingCampaignId");
		selectDropDownUsingIndex(marketingId, 2);

		type(locateElement("id", "createLeadForm_primaryEmail"),"manos.29.raja@mail.com");
		type(locateElement("id", "createLeadForm_primaryPhoneNumber"),"2586361");
		WebElement submitButton = locateElement("name", "submitButton");
		click(submitButton);

		//closeBrowser();

	}
}
