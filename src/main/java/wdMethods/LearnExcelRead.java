package wdMethods;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class LearnExcelRead {

	public static void main(String[] args) throws IOException {
		//book locate
		XSSFWorkbook wbook = new XSSFWorkbook("./data/createlead.xlsx");
		//sheet locate
		XSSFSheet sheet = wbook.getSheet("createlead");
		int lastRowNum = sheet.getLastRowNum();
		short lastCellNum = sheet.getRow(0).getLastCellNum();
		System.out.println(lastRowNum);
		System.out.println(lastCellNum);
		
		for (int j = 1; j <= lastRowNum; j++) {
			XSSFRow row = sheet.getRow(j);
			for (int i = 0; i <lastCellNum ; i++) {
				XSSFCell cell = row.getCell(i);
				String stringCellValue = cell.getStringCellValue();
				System.out.println(stringCellValue);
			} 
		}
		wbook.close();
	}

}
