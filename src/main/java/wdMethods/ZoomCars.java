package wdMethods;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

@Test
public class ZoomCars extends SeMethods {

	@BeforeClass
	public void setData() {
		testCaseName="TC001";
		testCaseDescription="Zoom Car Booking";
		author="Ram";
		category="Functional";
	}


	public void bookcar() throws InterruptedException {
		startApp("chrome","https://www.zoomcar.com/chennai/");
		WebElement start = locateElement("linkText", "Start your wonderful journey");
		click(start);
		WebElement location = locateElement("xpath", "(//div[@class = 'component-popular-locations']/div[@class = 'items'])[4]");
		click(location);
		WebElement next = locateElement("class", "proceed");
		click(next);
		WebElement datepick =  locateElement ("xpath", "( //div[@class = 'days']/div[@class = 'day'])[1]");
		click(datepick);
		WebElement proceed = locateElement ("class", "proceed");
		click(proceed);
		WebElement dropdate = locateElement("xpath", "( //div[@class = 'days']/div[@class = 'day'])[1]");
		click(dropdate);
		String text1 = getText(dropdate);
		WebElement pickeddropdate = locateElement("xpath", "//div[@class = 'days']/div[@class = 'day picked ']");
		String text2 = getText(pickeddropdate);
		if(text1.equalsIgnoreCase(text2))
		{
			System.out.println("The date selected by user and date picked by system are matching");
		}
		else {
			System.out.println("date is picked wrongly");
		}

		WebElement proceed1 = locateElement("class", "proceed");
		click(proceed1);
		Thread.sleep(5);
		List<WebElement> carlist = driver.findElementsByClassName("car-listing");
		int carcount = carlist.size();
		System.out.println("The number of cars available for your slot is " + carcount);
		
		WebElement sort = locateElement("xpath", "(//div[@class = 'sort-bar car-sort-layout']/div[@class = 'item'])[1]");
		click(sort);
		WebElement highestcar = locateElement("xpath", "( //div[@class = 'car-amount']/div[@class = 'price'])[1]");
		String rate = getText(highestcar);
		System.out.println("The highest price of car for your search is " + rate);
		WebElement book = locateElement("xpath", "(//button[@class = 'book-car'])[1]");
		click(book);
		closeBrowser();
		





	}
}

