package wdMethods;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.sound.midi.SysexMessage;

import org.apache.commons.io.FileUtils;
import org.apache.poi.util.SystemOutLogger;
import org.etsi.uri.x01903.v13.impl.GenericTimeStampTypeImpl;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

public class SeMethods extends LearnReports implements WdMethods{
	public int i = 1;
	public RemoteWebDriver driver;
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")){
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver = new ChromeDriver();
			} else if (browser.equalsIgnoreCase("firefox")){
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			System.out.println("inittialised");
			stepToReport("The Browser Launched Successfully","PASS");
		} catch (WebDriverException e) {
			//System.err.println("The Browser "+browser+" not Launched");
			stepToReport("The Browser \"+browser+\" not Launched","FAIL");
		} finally {
			takeSnap();
		}

	}

	
	public WebElement locateElement(String locator, String locValue) {
		try {
			switch(locator) {
			case "id"	 : return driver.findElementById(locValue);
			case "class" : return driver.findElementByClassName(locValue);
			case "xpath" : return driver.findElementByXPath(locValue);
			case "linkText" :return driver.findElementByLinkText(locValue);
			case "name" : return driver.findElementByName(locValue);
			case "partialLinkText" : return driver.findElementByPartialLinkText(locValue);
			case "tagName" : return driver.findElementByTagName(locValue);
			
			}
		} catch (NoSuchElementException e) {
			System.err.println("The Element is not found");
			stepToReport("The Element is not found","FAIL");
			
		} catch (Exception e) {
			System.err.println("Unknow Exception ");
			stepToReport("Unknow Exception ","FAIL");
			
			
		}
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		try {
			return driver.findElementById(locValue);
		} catch (NoSuchElementException e) {
			System.err.println("The Element is not found");
			stepToReport("The Element is not found","FAIL");
			
		}
		return null;
	}

	
	public void type(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data);
			System.out.println("The data "+data+" is Entered Successfully");
			stepToReport("The data "+data+" is Entered Successfully","PASS");
			
		} catch (WebDriverException e) {
			System.out.println("The data "+data+" is Not Entered");
			stepToReport("The data "+data+" is Not Entered","FAIL");
			
		} finally {
			takeSnap();
		}
	}
     
	
	
	public void clickWithNoSnap(WebElement ele) {
		try {
			ele.click();
			System.out.println("The Element "+ele+" Clicked Successfully");
			stepToReport("The Element "+ele+" Clicked Successfully","PASS");
			
		} catch (Exception e) {
			System.err.println("The Element "+ele+"is not Clicked");
			stepToReport("The Element "+ele+"is not Clicked","FAIL");
			
		}
	}
	
	
	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			System.out.println("The Element "+ele+" Clicked Successfully");
			stepToReport("The Element "+ele+" Clicked Successfully","PASS");
			
		} catch (WebDriverException e) {
		System.err.println("The Element "+ele+"is not Clicked");
		stepToReport("The Element "+ele+"is not Clicked","FAIL");
		
		} finally {
			takeSnap();
		}
	}

	@Override
	public String getText(WebElement ele) {
		try {
			
			System.out.println("text returned "+ ele.getText());
			stepToReport("text returned "+ ele.getText(),"PASS");
			
			return ele.getText();
		} catch (NoSuchElementException e) {
			System.err.println("The Element "+ele+"is not foung to find text");
			stepToReport("The Element "+ele+"is not foung to find text","FAIL");
			
		}
		
		return null;
		
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select dd = new Select(ele);
			dd.selectByVisibleText(value);
			System.out.println("The DropDown Is Selected with VisibleText "+value);
			stepToReport("The DropDown Is Selected with VisibleText","PASS");
			
		} catch (Exception e) {
			System.err.println("The DropDown Is not Selected with VisibleText "+value);
			stepToReport("The DropDown Is not Selected with VisibleText","FAIL");
			
		} finally {
			takeSnap();
		}

	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
			Select dd = new Select(ele);
			dd.selectByIndex(index);;
			System.out.println("The DropDown Is Selected with index "+index);
			stepToReport("The DropDown Is Selected with index "+index,"PASS");
			
		} catch (Exception e) {
			System.err.println("The DropDown Is not Selected with index "+index);
			stepToReport("The DropDown Is not Selected with index "+index,"FAIL");
			
		} finally {
			takeSnap();
		}


	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		try {
			String actual  = driver.getTitle();

			if (actual.equals(expectedTitle)) {
				return true;
			}
			else	
				return false;
			
	 
		} catch (Exception e) {
			System.err.println("unknown exception in getting Title");
			stepToReport("unknown exception in getting Title","FAIL");
			
			
		}
		return false;
		
		
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		try {
			String realText= ele.getText();
			if(realText.equals(expectedText)) {
				System.out.println("Exactly text is matching");
				stepToReport("Exactly text is matching","PASS");
				
			}
			else {
				System.out.println("Text is mismatching");
				stepToReport("Text is mismatching","PASS");
				

			}
		} catch (Exception e) {
			System.err.println("Unknown Error");
			stepToReport("Unknown Error","FAIL");
			
		}

	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		try {
			String userText = ele.getText();
					if (userText.contains(expectedText))
					{
						System.out.println("Your text is present in the expected place");
						stepToReport("Your text is present in the expected place","PASS");
						
					}

					else {
						System.out.println("Text not present");
						stepToReport("Text not present","PASS");
						
					}
		} catch (Exception e) {
			
			System.out.println("unknown error");
			stepToReport("Unknown Error","FAIL");
			
		}
	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToWindow(int index) {
		
		
		try {
			Set<String> allwindow = driver.getWindowHandles();
			List<String> listwindow = new ArrayList<String>();
			listwindow.addAll(allwindow);
			String secondwindow = listwindow.get(index);
			driver.switchTo().window(secondwindow);
			System.out.println("Window switched to " + index);
			stepToReport("Window switched to " + index,"PASS");
			
			
		} catch (Exception e) {
			System.err.println("Unable to switch");
			stepToReport("Unable to switch" + index,"FAIL");
			
		}
		
		

	}

	@Override
	public void switchToFrame(WebElement ele) {
		try {
		driver.switchTo().frame(ele);
		// TODO Auto-generated method stub
		stepToReport("frame switched to " + ele,"PASS");
		
		}
		catch(Exception e) {
			stepToReport("frame  not switched to " + ele,"Fail");
			
		}

	}

	@Override
	public void acceptAlert() {
		try {
		driver.switchTo().alert().accept();
		// TODO Auto-generated method stub
		stepToReport("Alert Accepted ","PASS");
		
		}
		catch(NoAlertPresentException e) {
			System.out.println("alert not found");
			stepToReport("alert not found" ,"Fail");
			
		}

	}

	@Override
	public void dismissAlert() {
		// TODO Auto-generated method stub
		try {
			driver.switchTo().alert().dismiss();
			stepToReport("Alert dismissed ","PASS");
			
			// TODO Auto-generated method stub
			}
			catch(NoAlertPresentException e) {
				System.out.println("alert not found");
				stepToReport("alert not dismissed" ,"Fail");
				
			}


	}

	@Override
	public String getAlertText() {
		try {
			String text = driver.switchTo().alert().getText();
			System.out.println("Alert text is "+text);
			stepToReport("Alert textis fetched ","PASS");
			
			return text;
			// TODO Auto-generated method stub
			}
			catch(NoAlertPresentException e) {
				System.out.println("alert not found");
				stepToReport("alert not found" ,"Fail");
				
			}
		     catch(Exception e) {
		    	 System.out.println("unknown error"); 
		    	 stepToReport("alert not found" ,"Fail");
					
		     }
		return null;
	}

	
	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./snap/img"+i+".png");
		try {
			FileUtils.copyFile(src, des);
		} catch (IOException e) {
			System.err.println("IOException");
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		driver.close();
	}

	@Override
	public void closeAllBrowsers() {
		driver.quit();
		// TODO Auto-generated method stub

	}

}
