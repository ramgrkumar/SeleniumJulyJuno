package wdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;

public class ProjectMethods extends SeMethods {
	
	@DataProvider(name = "fetchData")
	public Object[][] getText() throws IOException{
		
		Object[][] excelData = ReadExcel.getExcelData(excelfilename);
		return excelData;
	}
	
	@BeforeMethod(groups="common")
	public void login() {
		startApp("chrome","http://leaftaps.com/opentaps/control/main");
		WebElement se1 = locateElement("id","username");
		type (se1,"DemoSalesManager");
		 se1 = locateElement("id","password");
		 type (se1,"crmsfa");
		  WebElement login = locateElement("class","decorativeSubmit");
		 click(login);
		 WebElement crmsfa = locateElement("linkText","CRM/SFA");
		 click(crmsfa);
	}
	
	@AfterMethod(groups="common")
	public void closebrowser() {
		closeBrowser();
	}
	
	

}
