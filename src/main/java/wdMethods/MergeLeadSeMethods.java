package wdMethods;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class MergeLeadSeMethods extends ProjectMethods {
	
	@BeforeClass
	public void setData() {
		testCaseName="TC002";
		testCaseDescription="Merge lead";
		author="Ram";
		category="Smoke";
	}
   @Test (enabled = false)
	public void  MergeLead () throws InterruptedException {
		login();

		WebElement leads = locateElement("linkText", "Leads");
		click(leads);
		WebElement mergeleads = locateElement("linkText", "Merge Leads");
		click(mergeleads);
		WebElement lookup1 = locateElement("xpath", "(//img[@alt= 'Lookup'])[1]");
		click(lookup1);
		Thread.sleep(3000);
		switchToWindow(1);

		WebElement id = locateElement("name", "id");
		type(id, "10123");
		WebElement button = locateElement("xpath", "//button[text() = 'Find Leads']");
		click(button);
		Thread.sleep(3000);
		WebElement ele1  = locateElement("xpath", "(//a[@class = 'linktext'])[1]");
		click(ele1);
		switchToWindow(0);
		WebElement lookup2 = locateElement("xpath", "(//img[@alt= 'Lookup'])[2]");
		click(lookup2);
		switchToWindow(1);
		WebElement id1 = locateElement("name", "id");
		type(id1, "10124");
		WebElement button1 = locateElement("xpath", "//button[text() = 'Find Leads']");
		click(button1);
		Thread.sleep(3000);

		WebElement ele2 = locateElement ("xpath", "(//a[@class = 'linktext'])[1]");
		click(ele2);
		switchToWindow(0);
		WebElement button3 = locateElement("xpath", "//a[@class = 'buttonDangerous']");
		click(button3);

		driver.switchTo().alert().accept();
		//String error = driver.findElementByClassName("errorMessage").getText();
		//System.out.println(error);

		closeBrowser();



	}
}