package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class EditLeadSeMethods extends ProjectMethods{
	
	@BeforeClass(groups = "common")
	public void setData() {
		testCaseName="TC004";
		testCaseDescription="Edit lead";
		author="Ram";
		category="Smoke";
	}
	
	@Test (/*dependsOnMethods = "wdMethods.CreateLeadSeMethods.testCreateLead"*/groups="sanity", dependsOnGroups="smoke")
	public void editleads() {
		
		login();
		 //Leads
		 WebElement lead = locateElement("linkText","Leads");
		 click(lead);
		//Find Leads
		 WebElement findLead = locateElement("linkText","Find Leads");
		 click(findLead);
		
		 
	   type(locateElement("xpath", "(//input[@name='firstName'])[3]"),"IBM");
	 
	WebElement findLeads = locateElement("xpath", "//button[text()='Find Leads']");
	click(findLeads);
		
	
			WebElement firstElement = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a");
			click(firstElement);
			verifyTitle("View Lead");
			WebElement editlead = locateElement("linkText", "Edit");
			click(editlead);
			type(locateElement("id", "updateLeadForm_companyName"),"IBM2");
			WebElement updatelead = locateElement("class", "smallSubmit");
			click(updatelead);
			//closeBrowser();
		
				
		
	}

}
