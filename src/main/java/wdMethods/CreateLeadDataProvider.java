package wdMethods;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CreateLeadDataProvider  extends ProjectMethods{
	
	@BeforeClass(groups = "common")
	public void setData() {
		testCaseName="TC001";
		testCaseDescription="Create lead using data provider";
		author="Ram";
		category="Smoke";
		excelfilename = "createlead";
		
	}

	@Test(/*invocationCount=2, invocationTimeOut=50000*/ groups = "smoke", dataProvider="fetchData")
	public void  testCreateLead (String cName, String fName, String lName, String email, String Phone) {
		//login();
		WebElement createlead = locateElement("linkText","Create Lead");
		click(createlead);


		type(locateElement("id", "createLeadForm_companyName"),cName);
		type(locateElement("id", "createLeadForm_firstName"), fName);
		type(locateElement("id", "createLeadForm_lastName"), lName);


		WebElement sourceDrop = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(sourceDrop, "Conference");
		WebElement marketingId = locateElement("id", "createLeadForm_marketingCampaignId");
		selectDropDownUsingIndex(marketingId, 2);

		type(locateElement("id", "createLeadForm_primaryEmail"), email);
		type(locateElement("id", "createLeadForm_primaryPhoneNumber"), Phone);
		WebElement submitButton = locateElement("name", "submitButton");
		click(submitButton);

}
	/*@DataProvider(name = "CreateLead")
	public Object[][] getText(){
		Object[][] data = new Object[2][5];
		data[0][0] = "IBM";
		data[0][1] = "Ramkumar";
		data[0][2] = "G";
		data[0][3] = "grkumar@tce.edu";
		data[0][4] = "9090192091";
		
		data[1][0] = "Verizon";
		data[1][1] = "mano";
		data[1][2] = "raja";
		data[1][3] = "mano@tce.edu";
		data[1][4] = "9090192901";
		
		return data;
		
		//ReadExcel.getExcelData("");
	}*/
	
	
	
}
