package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class EditLeadDataProvider extends ProjectMethods{
	
	@BeforeClass(groups = "common")
	public void setData() {
		testCaseName="TC002";
		testCaseDescription="Edit lead using data provider";
		author="Ram";
		category="Smoke";
	}
	
	@Test (/*dependsOnMethods = "wdMethods.CreateLeadSeMethods.testCreateLead"groups="sanity", dependsOnGroups="smoke"*/dataProvider="EditLead")
	public void editleads(String fName, String cName) {
		
		login();
		 //Leads
		 WebElement lead = locateElement("linkText","Leads");
		 click(lead);
		//Find Leads
		 WebElement findLead = locateElement("linkText","Find Leads");
		 click(findLead);
		
		 
	   type(locateElement("xpath", "(//input[@name='firstName'])[3]"),fName);
	 
	WebElement findLeads = locateElement("xpath", "//button[text()='Find Leads']");
	click(findLeads);
		
	
			WebElement firstElement = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a");
			click(firstElement);
			verifyTitle("View Lead");
			WebElement editlead = locateElement("linkText", "Edit");
			click(editlead);
			type(locateElement("id", "updateLeadForm_companyName"), cName);
			WebElement updatelead = locateElement("class", "smallSubmit");
			click(updatelead);
			//closeBrowser();	
	}

	@DataProvider(name = "EditLead")
	public Object[][] getText(){
		Object[][] data = new Object[2][2];
		data[0][0] = "Ramkumar";
		data[0][1] = "Zoho";
		
		data[1][0] = "Mano";
		data[1][1] = "Facebook";

		
		return data;
	}
	
}
