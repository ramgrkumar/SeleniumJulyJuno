package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class DeleteLeadDataProvider extends ProjectMethods {
	
	@BeforeClass(groups = "common")
	public void setData() {
		testCaseName="TC003";
		testCaseDescription="Delete lead using data provider";
		author="Ram";
		category="Smoke";
	}
	@Test(/*dependsOnMethods = "wdMethods.EditLeadSeMethods.editleads"groups="regression", dependsOnGroups = "sanity"*/ dataProvider = "DeleteLead")
	public void deleteLead(String Phone) {
		
		//login();
		 WebElement lead = locateElement("linkText","Leads");
		 click(lead);
		//Find Leads
		 WebElement findLead = locateElement("linkText","Find Leads");
		 click(findLead);
	
		 WebElement phone = locateElement("xpath","//span[text()='Phone']");
		 click(phone);
		
		 
		 type(locateElement("xpath", "//input[@name='phoneNumber']"), Phone);
		 WebElement findLeads = locateElement("xpath", "//button[text()='Find Leads']");
			click(findLeads);
			WebElement firstElement = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a");
			click(firstElement);
			//Delete
			WebElement editlead = locateElement("linkText", "Delete");
			click(editlead);
			//closeBrowser();
	}
	@DataProvider(name = "DeleteLead")
	public Object[][] getText(){
		Object[][] data = new Object[2][1];
		data[0][0] = "1";
		
		data[1][0] = "9";

		
		return data;
	}
	
	
	
}
