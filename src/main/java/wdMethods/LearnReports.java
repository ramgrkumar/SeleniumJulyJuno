package wdMethods;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnReports {
	
	public static ExtentReports extent;
	public static ExtentTest test;
	public static String testCaseName, testCaseDescription,author, category, excelfilename;

	@BeforeSuite(groups="common")
	public void startReport() {
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html"); 
		html.setAppendExisting(true); //to track the history of run
	 extent = new ExtentReports();
		extent.attachReporter(html);
	}
	
@BeforeMethod(groups="common")
	public void startTestCase(){
	 test = extent.createTest(testCaseName, testCaseDescription);
	test.assignAuthor(author);
	test.assignCategory(category);
	}


 public void stepToReport(String testStepDesc, String Status) {
	 
	 System.out.println(testStepDesc);
	 if(Status.equalsIgnoreCase("PASS")) {
	 test.pass(testStepDesc);

	 }
	 else if(Status.equalsIgnoreCase("FAIL")) {
		 test.fail(testStepDesc);
 
	 }
 }

	    @AfterSuite(groups="common")
		public void endOfReport() {
			extent.flush();
		}
		
	
		
		

	}


